export interface User {
  firstName: string;
  secondName: string;
  emailAdress: string;
  telephone: string;
  textPole: string;
}
