import { Component } from "@angular/core";
import { User } from "./signup.interface";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-root",
  templateUrl: "./app.template.html",
  styleUrls: ["app.style.css"]
})
export class AppComponent {
  public mask = [
    "7",
    "(",
    /[1-9]/,
    /\d/,
    /\d/,
    ")",
    " ",
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];

  user: User = {
    firstName: "",
    secondName: "",
    emailAdress: "",
    telephone: "",
    textPole: ""
  };

  constructor(private http: HttpClient) {}

  onSubmit({ value }: { value: User }) {
    this.http.post("http://frontfox.ru:3004/data_from_form", value).subscribe(
      val => {
        console.log("POST call successful value returned in body", val);
      },
      response => {
        console.log("POST call in error", response);
      },
      () => {
        console.log("The POST observable is now completed.");
      }
    );
  }
}
